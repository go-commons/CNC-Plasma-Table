# CNC Torch Table product Requirements
- [Overview of current design issue#28](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/28)
- [Comments on good quality cuts issue#27](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/27)
## 1 Customizable design
### 1.1. Variable BOMs based on particular needs
- Related issues: [#45](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/45),[#11](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/11)
**Variable shipping costs**
**Variable**
## 2 Design for manufacturing
### 2 Easy to assemble
#### 2.1 Modular design
- This implies loose coupling of parts and components, joining profiles with screws instead of welding.
 - This is why the aluminum profiles have been selected for the current version.
 - We use as much as possible the same structural components.

Related issues: [#35](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/35), [#30](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/30), [#40](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/40)

#### 2.2 Easy to wire (electrical connections)
Related issues: [#16](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/16)
#### 2.3 Made with off the shelve hardware

## 3 Usage centered design
- Space management under table issue [#8](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/8)
- Kit design product feedback [#48](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/48)
### 3 Safe to operate
#### 3.1 Protect user from sparks
- **Water table option** The water table makes the
Related issues:[#7](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/7)
- **Dust collector**

#### 3.2 Protect user from electrical injury
- **This is related to plasma cutter selection**. Select a plasma cutter that is CNC ready. These kinds of plasma cutters have a special connector for CNC tables.

## 4 Functional requirements
### 4.1 Torch motion (x,y,z)
Related issues: [#46](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/46),[#13](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/13)


### 4.2 Robust design (noise reduction)
> Read more about [robust product design](http://www.public.iastate.edu/~vardeman/IE361/s00mini/maurer.htm)

Related issues: [#37](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/37),[#43](https://gitlab.com/go_commons/CNC-Plasma-Table/issues/43)

**REFERENCES**
- [This video](https://youtu.be/8OTwT0WFBmg?t=157) explains the structural properties of certain types of aluminum profiles. (Assembly time of the table shown 4 hours)
